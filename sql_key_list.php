<?php

/*
************************************ MY SQL CODING PROJECT WITH DETAILS***********************************


1-> CREATE SQL CODE:-   

1. create database - (key)   {create database Student}


2. set database CHARACTER - (key)  {CHARACTER SET utf8 COLLATE utf8_unicode_ci}


3.  create table - (key)  {create table user_table()}


4. ste colum for table - (key) {[this one always need to be set] - id int (13 {char limit} primary key AUTO_INCREMENT - [this one always need to be set]),}

name varchar [data_types] (60),

There are so many data_types. We can use all of those for any database.it's depend on what type of database you wanna create. 

[This is the main concept for making table and colum with different types of data_type, char limit]

----------------------------------------------------------------------------------------------------------

2-> INSERT SQL CODE:-


1. INSERT INTO `table_name` (`id`, `name`, `email` [colum name]) VALUES (give you all colum value)

REAL EXAMPLE 
{
INSERT INTO `user_table` (`id`, `name`, `email`) VALUES
(1, 'Rahim', 'rahim@gmail.com'),
(2, 'Karim', 'karim@gmail.com');

}

----------------------------------------------------------------------------------------------------------

3-> ALTER SQL CODE:-

1. in alter you cann any colum or you can mopdify anything. For now I just add a new colum 


REAL EXAMPLE:- 

ALTER TABLE user_profile add COLUMN user_id int (13) UNIQUE key after id

DETAILS:-

{ALTER TABLE [table name]->user_profile add COLUMN [colum name]->user_id int (13) [set key for your new colum]-> UNIQUE key [set placement {you can use after or first key just type key and declar colum name}] -> after id}

----------------------------------------------------------------------------------------------------------

4->  RELATIONSHIP SQL KEY:- 

ALTER table user_profile add FOREIGN KEY (user_id) REFERENCES user_table(id)

This is the basic relationship sql key code.


DETAILS:- ALTER table [table name]-> user_profile [add_key]-> add FOREIGN KEY  [FOREIGN key colum name]->(user_id) REFERENCES [REFERENCES table name and colum name]user_table(id)

----------------------------------------------------------------------------------------------------------


There are 3 types of relationship in sql.

1.one to one
2.one to many
3.many to many

Now i will show all of those relationship code step by step wih real example and  details:-

ONE TO ONE RELATIONSHIP:-

{use banglish for me}

one to one relationship add krte hle first sql code use krte hbe.

like this {ALTER table user_profile add FOREIGN KEY (user_id) REFERENCES user_table(id)}

one to one relationship e foreign key always uniqe key heti hbe.

EXAMPLE:-

user_table       [Relationship]        user_profile
    <>---------------------------------<>
    id                               user_id
     <REFERENCE KEY>                    <FOREIGN KEY> [always unique]


              --This is the basic stracture of one to one relationship--


{
project e relationship clear korar korar jonno databse ei relation kore form value  er concept korlam
id er sathe user id relationship kore user_id te id er value set kore. tar jonno modify key use korlam 

like this [UPDATE user_profile SET user_id = 1 where id = 2]

Details:- {UPDATE-> [key] [table_name]-> user_profile SET -> [key] [colum name] ->user_id = [value]-> 1 where id = 2 [id number]}
}




----------------------------------------------------------------------------------------------------------




ONE TO MANY RELATIONSHIP:-

one to many relationship add krte hle first sql code use krte hbe.

like this {ALTER table user_profile add FOREIGN KEY (user_id) REFERENCES user_table(id)}

one to many relationship e foreign key always index key heti hbe.tar jonno key add krte hbe sql code use kore

like this:-  CREATE INDEX batch_id
ON students (batch_id);



EXAMPLE:-

batch        [Relationship]       students
    <>---------------------------------<>
    id                               batch_id
     <REFERENCE KEY>                    <FOREIGN KEY> [always index]


              --This is the basic stracture of one to manyrelationship--


{ This part  is same as one to one }

----------------------------------------------------------------------------------------------------------




MANY TO MANY RELATIONSHIP:-


many to many relationship add krte hle first sql code use krte hbe.

like this {ALTER table user_profile add FOREIGN KEY (user_id) REFERENCES user_table(id)}

many to many relationship e foreign key always index key heti hbe.tar jonno key add krte hbe sql code use kore

like this:-  CREATE INDEX batch_id
ON students (batch_id);

many to many relationship krte hle alada table korte hbe tar majhe dui ta colum korte hbe.oi dui colum ke
FOREIGN KEY hishabe use krte hbe r onno 2 table er specefic id colum k REFERENCES hishabe use krte hbe.





EXAMPLE:-

course               students
    <>                               <>
    id                               id
     <REFERENCE KEY>                    <REFERENCE KEY>


**********************************************************************************************************

                             Link_Table
        
                             CREATE RELATIONSHIP

                          
               COURSE er id->COURSE_ID      STUDENTS er id->STUDENTS_ID   
               
**********************************************************************************************************



<>                                                   <>
course_id                                            students_id   
<FOREIGN KEY> [always index]                <FOREIGN KEY> [always index]


              --This is the basic stracture of one to manyrelationship--


{
This time it's defferent. many to many te link table use krte hy tai id gulor value pete hole
insert use kre tader value ta dite hbe tahole value gulo link hoye jabe.


like this:-   


[INSERT INTO `admission_list` (`students_id`, `course_id`) VALUES
(2, 2),
(3, 1),
(1, 2),
(4, 1),
(2, 2),
(3, 1),
(1, 2),
(4, 1);]

}



********************************************END***********************************************************

*/